Source: vulkan-validationlayers
Priority: optional
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Uploaders: Timo Aaltonen <tjaalton@debian.org>
Build-Depends: debhelper (>= 11),
 cmake,
 glslang-dev (>= 7.10.2933),
 libvulkan-dev (>= 1.1.92.1),
 libwayland-dev,
 libx11-dev,
 libxrandr-dev,
 pkg-config,
 python3,
 spirv-tools (>= 2018.5+git20181102),
Standards-Version: 4.2.0
Section: libs
Homepage: https://github.com/KhronosGroup/Vulkan-ValidationLayers
Vcs-Git: https://salsa.debian.org/xorg-team/vulkan/vulkan-validationlayers.git
Vcs-Browser: https://salsa.debian.org/xorg-team/vulkan/vulkan-validationlayers

Package: vulkan-validationlayers
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: vulkan-loader,
 libvulkan1 (<< 1.1.92.1),
Replaces: vulkan-loader,
 libvulkan1 (<< 1.1.92.1),
Multi-Arch: same
Description: Vulkan validation layers
 This project provides the Khronos official Vulkan validation layers.
 .
 This package includes the loader library.

Package: vulkan-validationlayers-dev
Section: libdevel
Architecture: linux-any
Depends:
 vulkan-validationlayers (= ${binary:Version}),
 ${misc:Depends},
Breaks:
 libvulkan-dev (<< 1.1.92.1),
Replaces:
 libvulkan-dev (<< 1.1.92.1),
Multi-Arch: same
Description: Vulkan validation layers -- development files
 This project provides the Khronos official Vulkan validation layers.
 .
 This package includes files needed for development.
